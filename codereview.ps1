function New-Function {
    [CmdletBinding()]
    param (
        [Parameter(Mandatory=$True)]
        [string]$ComputerName
    )

    PROCESS {
        Test-Connection `
            -TargetName $ComputerName
    }
}